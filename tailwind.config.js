const defaultTheme = require("tailwindcss/defaultTheme");
const { colors: defaultColors } = require("tailwindcss/defaultTheme");

const colors = {
  ...defaultColors,
  ...{
    white: "#ffffff",
    transparentWhite: "#ffffffcc",
    semiTransparentWhite: 'rgba(255,255,255,0.3)',
    blue: {
      lightest: "#F5F8FB",
      light: "#EBF1F6",
      DEFAULT: "#B7D8FA",
    },
    purple: {
      dark: "#271D5D",
      DEFAULT: "#523B9A",
      light: "#764AFF",
      lightest: "#EAECF6",
    },
    grey: {
      light: "#969494",
    },
    knodsOrange: { DEFAULT: "#FF8A65" },
    knodsPurple: { DEFAULT: "#C7B9FF" },
    knodsYellow: { DEFAULT: "#FDEF7E" },
    zineWhite: { DEFAULT: '#FAF6D2'},
    zineBlue: {DEFAULT: '#0878BE'},
    zineBlack: {DEFAULT: '#1D1D1B'},
    zineGreen: {DEFAULT: '287586'}
  },
};

module.exports = {
  purge: {
    enabled: false,
    content: ["./public/index.html"],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ["Work Sans", ...defaultTheme.fontFamily.sans],
    },
    colors: colors,
    backgroundPosition: {
      bottom: "bottom",
      center: "center",
      left: "left",
      "left-bottom": "left bottom",
      "left-top": "left top",
      right: "right",
      "right-bottom": "right bottom",
      "right-top": "right top",
      top: "top",
      "left-bottom-6": "6rem bottom",
    },
    extend: {
      backgroundImage: (theme) => ({
        "home-banner": "url('img/knods-banner-bg.svg')",
        "block-banner": "url('img/block-violet-bg.svg')",
        "knods-talent": "url('img/knods-talent-bg.svg')",
        "zine-banner": "url('img/hero-banner.png')",
        saska: "url('img/saska.png')",
        geometry: "url('img/geometry.png')",
      }),
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
