Untuk menjalankan website ini di local:

1. Clone repo ini
2. Install Nodejs versi terbaru
3. Di direktori tempat repo ini, jalankan perintah
```
npm install
npm run dev
```
4. Website bisa diakses di localhost:8000/public/staging.html
5. Jika melakukan perubahan konfigurasi Tailwind, hentikan dev server dan jalankan perintah
```
npm run build
npm run dev
```
